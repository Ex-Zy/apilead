//jquery
window.$ = require("jquery");
window.jQuery = require("jquery");

// svg polyfil
import svg4everybody from 'svg4everybody';
svg4everybody();

// custom scrollbar
import './lib/perfect-scrollbar.jquery.min.js';

//common scripts
require('./_index.js');